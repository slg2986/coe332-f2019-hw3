from flask import Flask, jsonify, request
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

# Error Handling
@app.errorhandler(404)
def not_found(error=None):
    message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp

@app.route('/')
def coe332():
    return jsonify(data)

# Instructors
@app.route('/instructors')
def get_instructors():
    return jsonify(data['instructors'])

# Instructors - Specify Integer Number
@app.route('/instructors/<int:id>')
def get_instructor_by_id(id):
    if id < 3:
        return jsonify(data['instructors'][id])
    else:
        return not_found()

# Meeting
@app.route('/meeting')
def get_meeting():
    return jsonify(data['meeting'])

# Meeting - Specify Field
@app.route('/meeting/<string:field>')
def get_meeting_field(field):
    return jsonify(data['meeting'][field])

# Assignments
@app.route('/assignments')
def get_assignments():
    return jsonify(data['assignments'])

# Assignments - Specify Integer Number and Field
@app.route('/assignments/<int:id>/<string:field>')
def get_assignments_by_id_and_url(id, field):
    return jsonify(data['assignments'][id][field])

# Post New Assignment
@app.route('/assignments', methods=["POST"])
def post_assignment():
    request_data = request.data.decode("utf-8")
    request_dictionary = json.loads(request_data)
    data['assignments'].append(request_dictionary)
    return jsonify(data['assignments'])
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)